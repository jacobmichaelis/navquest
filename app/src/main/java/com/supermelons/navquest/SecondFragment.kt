package com.supermelons.navquest

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supermelons.navquest.MainActivity.Companion.TAG
import com.supermelons.navquest.databinding.SecondFragmentBinding

class SecondFragment : Fragment() {
    private val activity: MainActivity
        get() = requireActivity() as? MainActivity ?: throw Exception("Fragment attached to wrong activity")

    private lateinit var binding: SecondFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.i(TAG, "SecondFragment (onCreateView)")
        binding = SecondFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "SecondFragment (onViewCreated)")
        super.onViewCreated(view, savedInstanceState)

        binding.secondButton.setOnClickListener { activity.goToFirst() }
    }

    /************ TEST AREA **************/

    override fun onAttach(context: Context) {
        Log.i(TAG, "SecondFragment (onAttach)")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "SecondFragment (onCreate)")
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        Log.i(TAG, "SecondFragment (onStart)")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "SecondFragment (onResume)")
        super.onResume()
    }

    override fun onPause() {
        Log.i(TAG, "SecondFragment (onAttach)")
        super.onPause()
    }

    override fun onDetach() {
        Log.i(TAG, "SecondFragment (onDetach)")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.i(TAG, "SecondFragment (onDestroy)")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.i(TAG, "SecondFragment (onDestroyView)")
        super.onDestroyView()
    }

    override fun onStop() {
        Log.i(TAG, "SecondFragment (onStop)")
        super.onStop()
    }
}
