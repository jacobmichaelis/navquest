package com.supermelons.navquest

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supermelons.navquest.MainActivity.Companion.TAG
import com.supermelons.navquest.databinding.FirstFragmentBinding

class FirstFragment : Fragment() {
    private val activity: MainActivity
        get() = requireActivity() as? MainActivity ?: throw Exception("Fragment attached to wrong activity")

    private lateinit var binding: FirstFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.i(TAG, "FirstFragment (onCreateView)")
        binding = FirstFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "FirstFragment (onViewCreated)")
        super.onViewCreated(view, savedInstanceState)

        binding.firstButton.setOnClickListener { activity.goToSecond() }

        binding.thirdButton.setOnClickListener { activity.goToThird() }
    }

    /************ TEST AREA **************/

    override fun onAttach(context: Context) {
        Log.i(TAG, "FirstFragment (onAttach)")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "FirstFragment (onCreate)")
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        Log.i(TAG, "FirstFragment (onStart)")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "FirstFragment (onResume)")
        super.onResume()
    }

    override fun onPause() {
        Log.i(TAG, "FirstFragment (onAttach)")
        super.onPause()
    }

    override fun onDetach() {
        Log.i(TAG, "FirstFragment (onDetach)")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.i(TAG, "FirstFragment (onDestroy)")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.i(TAG, "FirstFragment (onDestroyView)")
        super.onDestroyView()
    }

    override fun onStop() {
        Log.i(TAG, "FirstFragment (onStop)")
        super.onStop()
    }
}
