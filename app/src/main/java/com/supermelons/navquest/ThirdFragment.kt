package com.supermelons.navquest

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supermelons.navquest.MainActivity.Companion.TAG
import com.supermelons.navquest.databinding.ThirdFragmentBinding

class ThirdFragment : Fragment() {
    private val activity: MainActivity
        get() = requireActivity() as? MainActivity ?: throw Exception("Fragment attached to wrong activity")

    private lateinit var binding: ThirdFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.i(TAG, "ThirdFragment (onCreateView)")
        binding = ThirdFragmentBinding.inflate(inflater, container, false)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.i(TAG, "ThirdFragment (onViewCreated)")
        super.onViewCreated(view, savedInstanceState)

        binding.logoutButton.setOnClickListener { activity.logout() }
    }

    /************ TEST AREA **************/

    override fun onAttach(context: Context) {
        Log.i(TAG, "ThirdFragment (onAttach)")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "ThirdFragment (onCreate)")
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        Log.i(TAG, "ThirdFragment (onStart)")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "ThirdFragment (onResume)")
        super.onResume()
    }

    override fun onPause() {
        Log.i(TAG, "ThirdFragment (onAttach)")
        super.onPause()
    }

    override fun onDetach() {
        Log.i(TAG, "ThirdFragment (onDetach)")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.i(TAG, "ThirdFragment (onDestroy)")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.i(TAG, "ThirdFragment (onDestroyView)")
        super.onDestroyView()
    }

    override fun onStop() {
        Log.i(TAG, "ThirdFragment (onStop)")
        super.onStop()
    }
}
