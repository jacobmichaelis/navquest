package com.supermelons.navquest

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.view.ViewTreeObserver.*
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.supermelons.navquest.databinding.MainActivityBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

private val Context.dataStore by preferencesDataStore(name = "MAIN_DATASTORE")

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var binding: MainActivityBinding
    private var loaded = false
    private var loggedIn = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(LayoutInflater.from(this))
        installSplashScreen()
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(binding.mainContent.id) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)

        findViewById<View>(android.R.id.content)?.apply {
            viewTreeObserver.addOnPreDrawListener { loaded }
        }

        lifecycleScope.launch {
            dataStore.data.map {
                it[booleanPreferencesKey(LOGGED_IN)] ?: false
            }.collectLatest {
                loggedIn = it
                if (loggedIn && !loaded) {
                    goToThird()
                }
                loaded = true
            }
        }
    }

    private suspend fun updateLoggedIn(status: Boolean) {
        dataStore.edit {
            it[booleanPreferencesKey(LOGGED_IN)] = status
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        Log.i(TAG, "=== MainActivity [onDestroy] ===")
        super.onDestroy()
    }

    fun goToSecond() {
        Log.i(TAG, "=== MainActivity [goToSecond] ===")
        navController.navigate(R.id.first_to_second)
    }

    fun goToFirst() {
        Log.i(TAG, "=== MainActivity [goToFirst] ===")
        navController.navigate(R.id.second_to_first)
    }

    fun goToThird() {
        Log.i(TAG, "=== MainActivity [goToThird] ===")
        navController.navigate(R.id.first_to_third)
        lifecycleScope.launch { updateLoggedIn(true) }
    }

    fun logout() {
        Log.i(TAG, "=== MainActivity [logout] ===")
        navController.navigate(R.id.logoutAction)
        lifecycleScope.launch { updateLoggedIn(false) }
    }

    override fun onBackPressed() {
        Log.i(TAG, "=== MainActivity [onBackPressed] ===")
        super.onBackPressed()
    }

    companion object {
        const val TAG = "[NAV_QUEST]"
        const val LOGGED_IN = "LOGGED_IN"
    }
}
